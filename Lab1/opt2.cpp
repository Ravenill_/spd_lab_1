#include "opt2.h"
#include "CmaxFinder.h"

int Opt2::findCmax(std::vector<Task>& taskVector, int counter)
{
	int tempC = 0;
	bool reset = true;
	int bestCmax = 0;
	std::vector<Task> tempTaskVector = taskVector;

	while (reset)
	{
		bestCmax = CmaxFinder::findCmax(taskVector, counter);
		for (int i = 0; i < counter - 1; i++)
		{
			for (int k = i + 1; k < counter; k++)
			{
				reset = false;
				std::swap(tempTaskVector[i], tempTaskVector[k]);
				tempC = CmaxFinder::findCmax(tempTaskVector, counter);
				if (tempC < bestCmax)
				{
					taskVector = tempTaskVector;
					bestCmax = tempC;
					reset = true;
					break;
				}
				else
				{
					tempTaskVector = taskVector;
				}
			}
			if (reset)
				break;
		}
	}
	return bestCmax;
}
