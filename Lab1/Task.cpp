#include "Task.h"

Task::Task()
: r(0)
, p(0)
, q(0)
{

}

std::istream& operator >> (std::istream& inputStream, Task& obj)
{
	inputStream >> obj.r;
	inputStream >> obj.p;
	inputStream >> obj.q;

	return inputStream;
}

std::ostream& operator << (std::ostream & inputStream, Task & obj)
{
	std::cout << "r:" << obj.r << " p:" << obj.p << " q:" << obj.q << "\n";
	return inputStream;
}
