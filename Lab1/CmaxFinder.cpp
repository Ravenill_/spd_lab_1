#include "CmaxFinder.h"

int CmaxFinder::findCmax(std::vector<Task>& _taskVector, int _counter)
{
	int c = _taskVector.front().r;
	int cMax = 0;
	int cPi = 0;

	for (int i = 0; ;)
	{
		c = c + _taskVector[i].p;
		cPi = c + _taskVector[i].q;

		if (cMax < cPi)
			cMax = cPi;

		if (++i >= _counter)
			break;

		if (_taskVector[i].r >= c)
			c = _taskVector[i].r;
	}

	return cMax;
}
