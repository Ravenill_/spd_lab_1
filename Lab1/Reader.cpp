#include "Reader.h"
#include <iostream>
#include <assert.h>

Reader::Reader(std::string fileName) : countOfTasks(0), countOfParameters(0)
{
	openFile(fileName);
}

Reader::Reader() : countOfTasks(0), countOfParameters(0)
{
	std::string fileName;
	std::cout << "Give me the name... Name of the file!\n";
	std::cin >> fileName;

	openFile(fileName);
}

void Reader::openFile(std::string fileName)
{
	file.open(fileName.c_str(), std::ios::in);
	if (!file.is_open())
	{
		std::cerr << "Warning! File not found\n";
		return;
	}
}

void Reader::readFromFileTo(std::vector<Task>& taskVectorStructure)
{
	file >> countOfTasks;
	file >> countOfParameters;

	for (int i = 0; i < countOfTasks; i++)
	{
		Task temp_task;
		file >> temp_task;
		taskVectorStructure.push_back(temp_task);
	}
}

int Reader::getCountOfTasks() const
{
	return countOfTasks;
}

int Reader::getCountOfParamteres() const
{
	return countOfParameters;
}
