#pragma once
#include <string>
#include <fstream>
#include <vector>
#include "Task.h"

class Reader
{
private:
	int countOfTasks;
	int countOfParameters;
	std::fstream file;	

public:
	Reader(std::string fileName);
	Reader();

	void openFile(std::string fileName);
	void readFromFileTo(std::vector<Task>& taskVectorStructure);

	int getCountOfTasks() const;
	int getCountOfParamteres() const;
};