#include "Reader.h"
#include "CmaxFinder.h"
#include <cstdlib>
#include "SortR.h"
#include "opt2.h"

void printCmax()
{
	std::cout << "\n";
	std::cout << "Value of Cmax: ";
}

int main()
{
	std::vector<Task> taskVect; //wektor, kt�ry przechowuje taski

	Reader reader; //obiekt czytaj�cy z pliku. W konstruktorze mo�esz poda� jego nazw�.
	reader.readFromFileTo(taskVect); //fatkycznie wczytanie z pliku do wektora

	std::cout << "Loaded data:\n";
	for (auto& obj : taskVect) //zwyk�e wy�wietlenie
		std::cout << obj;

	printCmax(); //printuje "Value of Cmax: "
	std::cout << CmaxFinder::findCmax(taskVect, reader.getCountOfTasks()) << std::endl;
	system("PAUSE"); //pauzuje konsole

	SortR::sort(taskVect);
	std::cout << "Sorted data:\n";
	for (auto& obj : taskVect) //zwyk�e wy�wietlenie
		std::cout << obj;

	printCmax(); //printuje "Value of Cmax: "
	std::cout << CmaxFinder::findCmax(taskVect, reader.getCountOfTasks()) << std::endl;
	system("PAUSE"); //pauzuje konsole

	printCmax(); //printuje "Value of Cmax: "
	std::cout << Opt2::findCmax(taskVect, reader.getCountOfTasks()) << std::endl;
	system("PAUSE");
	std::cout << "Resorted data:\n";
	for (auto& obj : taskVect) //zwyk�e wy�wietlenie
		std::cout << obj;

	system("PAUSE");
}
