#pragma once
#include <vector>
#include "Task.h"

class CmaxFinder
{
public:
	static int findCmax(std::vector<Task>& _taskVector, int _counter);
};