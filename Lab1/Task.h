#pragma once
#include <iostream>

class Task
{
public:
	int r;
	int p;
	int q;

	Task();

	friend std::istream& operator >> (std::istream& inputStream, Task& obj);
	friend std::ostream& operator << (std::ostream& outputStream, Task& obj);
};